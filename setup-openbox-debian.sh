#!/bin/bash
vbox_version=""
vbox_version_regex=[0-9]+\.[0-9]+\.[0-9]+
version_asked=0
install_pcmanfm=false
install_alsa_utils=false
install_firefox=false
configure_menu=false
configure_rc=false
configure_date_time=false
install_guest_additions=false
reboot=false

print_msg() {
  green=$(tput setaf 2)
  red=$(tput setaf 1)
  yellow=$(tput setaf 11)
  prefix="$(tput bold)"
  sufix="$(tput sgr0)"
  color=""

  if [ ! -z $2 ]; then
    case $2 in
      warning)
        prefix="${yellow}${prefix}"
        color="${yellow}"
        ;;
      error)
        prefix="${red}${prefix}"
        color="${red}"
        ;;
      *)
        prefix="${green}${prefix}"
        color="${green}"
        ;;
      esac
      print_type=$2
  else
    print_type="echo"
  fi

  echo "[${prefix}${print_type^^}${sufix}]: ${color}$1${sufix}"
}

# Update system
print_msg "Updating the system" info
sudo apt update

# Install X and Desktop Envoirnment
print_msg "Installing X and Desktop Envoirnment" info
sudo apt install --no-install-recommends wget xorg openbox obconf dialog python3 -y

cmd=(dialog --separate-output --checklist "Plese select options:" 22 76 16)
options=(1 "Install PCManFM lightweight file manager" off
         2 "Install alsa-utils (alsamixer) for sound settings" off
         3 "Install Firefox ESR web browser" off
         4 "Configure menu" off
         5 "Configure rc file" off
         6 "Configure date and time" off
         7 "Install VirtualBox Guest Additions" off)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
  case $choice in
    1)
      install_pcmanfm=true
      ;;
    2)
      install_alsa_utils=true
      ;;
    3)
      install_firefox=true
      ;;
    4)
      configure_menu=true
      ;;
    5)
      configure_rc=true
      ;;
    6)
      configure_date_time=true
      ;;
    7)
      install_guest_additions=true
  esac
done

# Start X on boot
if [ ! -f ~/.bash_profile ]; then
  print_msg "Creating ~/.bash_profile file" info
  touch ~/.bash_profile
fi

found=$(grep -cw 'startx' ~/.bash_profile)
if [ ! $found -gt 0 ]; then
  print_msg "Adding startx to .bash_profile file" info
  echo startx >> ~/.bash_profile
fi

if $install_pcmanfm ; then
  print_msg "Installing PCManFM" info
  sudo apt install -y pcmanfm
fi

if $install_alsa_utils ; then
  print_msg "Installing alsa-utils" info
  sudo apt install -y alsa-utils
  alsamixer
fi

if $install_firefox ; then
  print_msg "Installing Firefox ESR" info
  sudo apt install -y firefox-esr
fi

if $configure_menu ; then
  print_msg "menu.xml to ~/.config/openbox/" copying
  if [ ! -d ~/.config/openbox ]; then
    mkdir -p ~/.config/openbox
  fi
  cp menu.xml ~/.config/openbox/menu.xml
  if [ ! $? = 0 ]; then
    print_msg "Couldn't copy menu.xml to ~/.config/openbox/" error
  fi
fi

if $configure_rc ; then
  print_msg "rc.xml to ~/.config/openbox/" copying
  if [ ! -d ~/.config/openbox ]; then
    mkdir -p ~/.config/openbox
  fi
  cp rc.xml ~/.config/openbox/rc.xml
  if [ ! $? = 0 ]; then
    print_msg "Couldn't copy rc.xml to ~/.config/openbox/" error
  fi
fi

if $configure_menu || $configure_rc ; then
  openbox --reconfigure
fi

if $configure_date_time ; then
  print_msg "Configuring date and time" info
  sudo dpkg-reconfigure tzdata
fi

if $install_guest_additions ; then
  while [[ ! $vbox_version =~ $vbox_version_regex ]]; do
    version_asked=$((version_asked+1))
    if [ $version_asked -gt 5 ]; then
      exit
    else
      read -p "Enter VirtualBox version: " vbox_version
    fi
  done

  if [[ $vbox_version =~ $vbox_version_regex ]]; then
    echo "VirtualBox version: $vbox_version"
  else
    exit
  fi

  vbox_iso_file="VBoxGuestAdditions_$vbox_version.iso"

  # Install recommended softwares to run VirtualBox Guest Additions
  print_msg "Installing recommended softwares to run VirtualBox Guest Additions: dkms, gcc" info
  sudo apt install dkms gcc -y

  # Download VirtualBox Guest Additions
  if [ ! -f $vbox_iso_file ]; then
    print_msg "Downloafing $vbox_iso_file" info
    wget https://download.virtualbox.org/virtualbox/$vbox_version/$vbox_iso_file
  fi

  # Install VirtualBox Guest Additions
  print_msg "Installing VirtualBox Guest Additions" info
  if [ -f $vbox_iso_file ]; then
    sudo mkdir /mnt_vbox
    sudo mount -o loop $vbox_iso_file /mnt_vbox
    cd /mnt_vbox
    sudo ./VBoxLinuxAdditions.run
    reboot=true
  else
    echo
    print_msg "Couldn't download VirtualBox Guest Additions for version $vbox_version" error
  fi
fi

echo
print_msg "Enjoy using your new Linux system!" info

if $reboot ; then
  echo
  read -p 'Press Enter to reboot...'
  sudo reboot
fi
