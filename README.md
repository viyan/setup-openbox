# Setup Openbox

**Minimal installation of Ubuntu/Debian and Openbox in VirtualBox**\
_Following instructions has been tested with:_
- _Host OS: Ubuntu 21.04 & Guest OS: Ubuntu 18.04_
- _Host OS: Ubuntu 22.04 & Guest OS: Ubuntu 18.04_
- _Host OS: Windows 10 & Guest OS: Ubuntu 18.04_
- _Host OS: Debian 12 & Guest OS: Debian 12_

**Contents**
1. [Get ISO file](#1-get-iso-file)
2. [Create new virtual machine](#2-create-new-virtual-machine)
3. [Mount ISO file](#3-mount-iso-file)
4. [Run virtual machine](#4-run-virtual-machine)
5. [Setup new virtual machine](#5-setup-new-virtual-machine)
6. [Linux installation](#6-linux-installation)
7. [Linux setup](#7-linux-setup)
- [Some keyboard shortcus](#some-keyboard-shortcus)
- [Links for more help](#links-for-more-help)

# 
### 1. Get ISO file
Choose Ubuntu or Debian
- Download Ubuntu ISO file [from here](https://help.ubuntu.com/community/Installation/MinimalCD).
- Download Debian ISO file [from here](https://www.debian.org/distrib/netinst).

# 
### 2. Create new virtual machine
- Open VirtualBox Manager.
- Create new virtual machine by clicking on 'New'.
- Enter name for new machine.
- Choose 'Linux' for Type and 'Ubuntu (64 bit)' or 'Debian (64 bit)' for Version and click 'Next'.
- Select the amount of Memory (RAM), minimum recommended is 1024MB, click 'Next'.
- Select 'Create a virtual hard disk now' and click 'Create'.
- Select 'VDI (Virtual Disk Image)' and click 'Next'.
- Select 'Dynamically allocated' and click 'Next'.
- Select the size of the virtual hard disk, 20GB would be the safe number for a smooth installation, and click 'Create'.

# 
### 3. Mount ISO file
- Right-click the new created virtual machine and select the 'Settings' option.
- In settings window left side bar click on 'Storage'.
- Under the 'Storage Drives' section, select the disc 'Empty' item.
- Under the 'Attributes' section, click the disc icon and select the 'Choose a disk file' button.
- Select the downloaded ISO file.
- Click the 'OK' button.

# 
### 4. Run virtual machine
Click the new created virtual machine and press Enter key or click 'Start' button on top of VirtualBox Manager

# 
### 5. Setup new virtual machine
- When you run a virtual machine for first time, VirtualBox might ask you to select start-up disk, ensure the ISO file has been selected and click 'Start'
- Select 'Install' in boot menu and press Enter

# 
### 6. Linux installation 
_Please note, following steps are for Ubuntu installation. The Debian installation looks a bit different but most of the steps are same._
- Select a language.
- Select a location.
- Select a locale for date, time and currency formats.
- Configure keyboard layout.
- Leave hostname as it is, if you are not sure what hostname will be use for.
- Choose a mirror of the Ubuntu archive, press Enter twice.
- Leave proxy blank, if you are not going to use proxy.
- Wait for set up users and passwords
  - Enter full name for user
  - Enter user name (remember the user name, it will be used for login)
  - Enter password and confirm it
- Configure time if it is necessary, otherwise continue by pressing 'Yes'.
- Wait for loading additional components.
- In 'Partition disks' dialog:
  - Select 'Guided - use entire disk'
  - Select virtual disk you created (usually there is only one choose).
  - When setup asks 'Write the changes to disks?', press 'Yes'.
- Wait for installing and configuring the base system.
- In 'PAM confgration' dialog select how you want the system updates automatically. _Recommended option is 'Install security update automatically'_.
- In 'Software selection' dialog, leave softwares unselected if you want to have the minimal and lightweight system.
- If setup asks for 'Install the GRUB boot loader to the master boot record?', select 'Yes' and press Enter.
- When installation finished, it will ask for remove the installation media and reboot.
  - In top menu of VirtualBox select 'Devices > Optical Drives > Remove disk from virtual drive'.
  - If VirtualBox asked for force ejection, click on 'Force Unmount'.
  - Press 'Continue' in installation setup and wait for virtual machine to reboot.

# 
### 7. Linux setup
- Start virtual machine if it is not started.
- \(This step is only for Debian\) Login as root and install sudo, replace \[USER\] with your user name:
```bash
apt install sudo
adduser [USER] sudo
exit
```
- Login to Linux by entering your username and password.
- Update system by running these commands: 
```bash
sudo apt update
sudo apt upgrade
```

- Install git to get setup codes from GitLab:
```bash
sudo apt install git -y
```

- Change directory to home and clone setup files:
```bash
cd ~
git clone https://gitlab.com/viyan/setup-openbox.git
```

- Run setup bash script and follow setup wizard, replace \[distribution\] with the Linux distribution you installed in lower case, e.g. ubuntu or debian:
```bash
cd setup-openbox/
chmod +x setup-openbox-[distribution].sh
./setup-openbox-[distribution].sh
```
- Wait for script to update and install necessary packages.
- Select options to install softwares or configure system.
  - **PCManFM** is a fast and efficient file manager
  - **alsa-utils** for accessing and adjusting sound in your virtual machine, if selected you will see the alsamixer for sound settings.
    - Arrow left and right for moving between items.
    - Arrow up and down to increase and decrease volume of item.
    - Press 'M' key to mute/unmute item.
    - Press 'Esc' key to exit alsamixer settings.
  - **Firefox** for safe browsering.
  - Select configure menu, if you want custom menu coming with this setup.
  - Select configure rc, if you want to setup the keyboard shortcuts in the end of this page.
  - Select configure date and time, if you want to ensure that system has set the time correctly.
  - **VirtualBox Guest Additions** enable you to control and monitor guest execution ([read more](https://www.virtualbox.org/manual/ch04.html)), if selected the script will ask for VirtualBox version which can be found in top menu 'Help > About VirtualBox'. _please note, the version should be in x.x.xx format, eg. 6.1.1 or 6.1.22_
  - In the end reboot system.

**That's it, reboot system if you didn't yet and enjoy your new minimal Linux virtual machine!**

#
### Some keyboard shortcus
If you choose configure rc.xml, you can use following keyboard shortcuts:

| Shortcut       | Task                          |
| -------------- | ----------------------------- |
| **Ctr+F10**    | Open system menu              |
| **Ctrl+Alt+X** | Open terminal                 |
| **Ctrl+Alt+B** | Open default browser          |
| **Ctrl+Alt+C** | Open chromium (if installed)  |
| **Ctrl+Alt+F** | Open PCManFM file manager     |
| **Alt+F7**     | Move active window            |
| **Alt+F8**     | Resize active window          |
| **Alt+F9**     | Minimize active window        |
| **Alt+F10**    | Toggle maximize active window |

To open other installed applications, press **Ctrl+F10** or right click on desktop and go to Applications.

#
### Links for more help
- [Installation/LowMemorySystems](https://help.ubuntu.com/community/Installation/LowMemorySystems)
- [Building a kiosk computer](https://thepcspy.com/read/building-a-kiosk-computer-ubuntu-1404-chrome/)
